//% color="#17959d" iconWidth=50 iconHeight=40
namespace wulinkpython {
    //% block="Motor[INDEX] run Direction[DIR] Speed[SD]" blockType="command"
    //% INDEX.shadow="dropdown" INDEX.options="INDEX" 
    //% DIR.shadow="dropdown" DIR.options="DIR"
    //% SD.shadow="range" SD.params.min=0 SD.params.max=255 SD.defl=200
    export function motorRun(parameter: any, block: any) {
        let index = parameter.INDEX.code;
        let dir = parameter.DIR.code;
        let sd = parameter.SD.code;
        Generator.addInclude("addIncludeHD_Dual_Motor", "#include <HD_Dual_Motor.h>");
        Generator.addObject(`addHD_Dual_Motor_${index}`, "HD_Dual_Motor", `motor_${index}(${index})`);
        Generator.addCode(`motor_${index}.motorRun(${dir}, ${sd});`)
    }

    //% block="Motor[INDEX] run Direction[DIR] Speed[SD]" blockType="command"
    //% INDEX.shadow="dropdown" INDEX.options="INDEX"
    export function motorStop(parameter: any, block: any) {
        let index = parameter.INDEX.code;
        Generator.addInclude("addIncludeHD_Dual_Motor", "#include <HD_Dual_Motor.h>");
        Generator.addObject(`addHD_Dual_Motor_${index}`, "HD_Dual_Motor", `motor_${index}(${index})`);
        Generator.addCode(`motor_${index}.motorStop();`)
    }

    //% block="Ultrasonic port[PIN]" blockType="reporter"
    //% PIN.shadow="dropdown" PIN.options="PIN"
    export function Ultrasonic(parameter: any, block: any) {
        let pin = parameter.PIN.code;
        Generator.addInclude("addIncludeHD_Ultrasonic_digit", "#include <HD_Ultrasonic_digit.h>");
        Generator.addObject(`addUltrasonic_digit_${pin}`, "Ultrasonic_digit", `myUltrasonic_${pin}(${pin});`);
        Generator.addCode(`(int)myUltrasonic_${pin}.distance_digit(400)`)
    }
}