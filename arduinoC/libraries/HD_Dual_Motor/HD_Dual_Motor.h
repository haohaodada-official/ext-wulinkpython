#ifndef _HD_DUAL_MOTOR_H_
#define _HD_DUAL_MOTOR_H_
#include "Arduino.h"

/**
* The user selects the 4-way dc motor.
*/
enum Motors {
    MA = 0x1,
    MB = 0x2
};

/**
* The user defines the motor rotation direction.
*/
enum Dir {
    //% blockId="CW" block="CW"
    CW = 1,
    //% blockId="CCW" block="CCW"
    CCW = -1,
};

class HD_Dual_Motor
{
    public:
        HD_Dual_Motor(Motors index);
        void motorRun(Dir direction, int speed);
        void motorStop(void);
    private:
        uint8_t _DIR_pin,_PWM_pin;
};

#endif