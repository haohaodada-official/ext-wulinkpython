#include "HD_Dual_Motor.h"

HD_Dual_Motor::HD_Dual_Motor(Motors index) {
    if (index == MA) {
        _DIR_pin = P13;
        _PWM_pin = P8;
    } else if (index == MB){
        _DIR_pin = P14;
        _PWM_pin = P9;
    }
    pinMode(_DIR_pin,OUTPUT);
    pinMode(_PWM_pin,OUTPUT);
}

void HD_Dual_Motor::motorRun(Dir direction, int speed) {
    uint8_t _state = 0;
    if (_DIR_pin == P14) {
        _state = 1;
    }
    if (direction > 0) {
        digitalWrite(_DIR_pin,_state);
    } else {
        digitalWrite(_DIR_pin,1-_state);
    }
    speed = abs(speed * 4); // map 255 to 1023
    if (speed > 1023) {
        speed = 1023;
    }
    analogWrite(_PWM_pin,speed);
}

void HD_Dual_Motor::motorStop(void) {
    analogWrite(_PWM_pin,0);
}