#ifndef _HD_ULTRASONIC_DIGIT_H_
#define _HD_ULTRASONIC_DIGIT_H_
#include "Arduino.h"

class Ultrasonic_digit {
    public:
        Ultrasonic_digit(uint8_t port);
    double distance_digit(uint16_t MAXcm);
    private:
        uint8_t _Ultrasonic_pin;
    long measure(unsigned long timeout);
    volatile bool _measureFlag;
    volatile long _lastEnterTime;
    volatile float _measureValue;
    unsigned long myPulseIn(uint32_t pin, uint32_t value, long maxDuration);
};

#endif