#include "HD_Ultrasonic_digit.h"

Ultrasonic_digit::Ultrasonic_digit(uint8_t port) {
    _Ultrasonic_pin = port;
    _lastEnterTime = millis();
    _measureFlag = true;
    _measureValue = 0;
}

long Ultrasonic_digit::measure(unsigned long timeout) {
    unsigned long duration;

    if (millis() - _lastEnterTime > 23) {
        _measureFlag = true;
    }

    if (_measureFlag == true) {
        _lastEnterTime = millis();
        _measureFlag = false;
        pinMode(_Ultrasonic_pin, OUTPUT);
        digitalWrite(_Ultrasonic_pin, LOW);
        delayMicroseconds(2);
        digitalWrite(_Ultrasonic_pin, HIGH);
        delayMicroseconds(10);
        digitalWrite(_Ultrasonic_pin, LOW);
        pinMode(_Ultrasonic_pin, INPUT);
        duration = myPulseIn(_Ultrasonic_pin, HIGH, timeout);
        _measureValue = duration;
    } else {
        duration = _measureValue;
    }
    return (duration);
}

double Ultrasonic_digit::distance_digit(uint16_t MAXcm) {
    long distance = measure(MAXcm * 55 + 200);
    if (distance == 0) {
        distance = MAXcm * 58;
    }
    return ((double) distance / 58.0);
}

unsigned long Ultrasonic_digit::myPulseIn(uint32_t pin, uint32_t value, long maxDuration)
{
  int pulse = value == HIGH ? 1 : 0;
  uint64_t tick =  micros();
  uint64_t maxd = (uint64_t)maxDuration;
  while(digitalRead(pin) != pulse) {
    if(micros() - tick > maxd)
      return 0;
  }
  uint64_t start =  micros();
  while(digitalRead(pin) == pulse) {
    if(micros() - tick > maxd)
      return 0;
    }
  uint64_t end =  micros();
  return end - start;
}